<?php

/**
 * Menu callback: Field manager admin page.
 */
function field_manager_admin() {
  $build = array();

  $build['role_title'] = array(
    '#markup' => t('Field management'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );
  $build['role_description'] = array(
    '#markup' => t('Administer field visibilty settings (for detail view, csv, json, etc.) per role and per content type.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $roles = user_roles();
  $types = field_manager_subset_allowed_node_types();

  foreach ($roles as $rid => $role) {
    $item = array();
    $item[] = $role;

    foreach ((array) $types as $type) {
      $item[] = l(t('Fields: @type', array('@type' => $type)), 'admin/structure/field-manager/roles/' . $rid . '/' . $type . '/fields');
    }

    $items[] = $item;
  }

  $header = array(t('Role'));

  foreach ((array) $types as $type) {
    $header[] = $type;
  }

  $build['role_list'] = array(
    '#theme' => 'table',
    '#rows' => $items,
    '#header' => $header,
  );

  $build['subset_title'] = array(
    '#markup' => t('Content type subset management'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );
  $build['subset_description'] = array(
    '#markup' => t('Administer content type derivatives.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $build['subset_add'] = array(
    '#markup' => l(t('Add new subset'), 'admin/structure/field-manager/subsets/add'),
  );

  $subsets = field_manager_subsets();
  $rows = array();
  if (!empty($subsets)) {
    foreach ($subsets as $subset) {
      $operations = array(
        l(t('Fields'), 'admin/structure/field-manager/subsets/' . $subset->id . '/fields'),
        l(t('Edit'), 'admin/structure/field-manager/subsets/' . $subset->id . '/edit'),
        l(t('Delete'), 'admin/structure/field-manager/subsets/' . $subset->id . '/delete'),
      );

      $rows[] = array(
        $subset->name,
        $subset->content_type,
        implode($operations, ' '),
      );
    }
  }

  $header = array(t('Name'), t('Content type'), t('Operations'));
  $build['subsets'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
    '#empty' => t('There are no subsets defined yet. <a href="@url">Create</a> the first.', array('@url' => url('admin/structure/field-manager/subsets/add'))),
  );

  return $build;
}

/**
 * Form callback: Field manager settings form.
 */
function field_manager_settings() {
  $form = array();

  $types = node_type_get_types();

  $options = array();
  foreach ($types as $key => $type) {
    $options[$key] = $type->name;
  }

  $form['field_manager_allowed_types'] = array(
    '#title' => t('Allowed node types'),
    '#description' => t('What node types should field manager operate on.'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('field_manager_allowed_types', array()),
  );

  return system_settings_form($form);
}

/**
 * Form callback: Subset form.
 */
function field_manager_subset_form($form, $form_state, $subset = NULL) {
  $form = array();

  $form['back'] = array(
    '#markup' => l(t('Back to overview'), 'admin/structure/field-manager'),
  );

  if (!empty($subset)) {
    $form['subset'] = array(
      '#type' => 'value',
      '#value' => $subset,
    );

    $new = FALSE;
    $wrapper_settings = array(
      '#title' => t('Edit subset', array(), array('context' => 'custom')),
    );
  }
  else {
    $new = TRUE;
    $wrapper_settings = array(
      '#title' => t('Add new subset', array(), array('context' => 'custom')),
    );
  }

  $form['wrapper'] = array(
    '#type' => 'fieldset',
  ) + $wrapper_settings;

  $form['wrapper']['content_type'] = array(
    '#title' => t('Content type'),
    '#description' => t('Select the content type for which you want to create a subset.'),
    '#type' => 'select',
    '#options' => field_manager_subset_allowed_node_types(),
    '#disabled' => ($new) ? FALSE : TRUE,
    '#default_value' => isset($subset) ? $subset->content_type : FALSE,
  );

  $form['wrapper']['name'] = array(
    '#title' => t('Name'),
    '#description' => t('Administrative name of the subset'),
    '#type' => 'textfield',
    '#default_value' => isset($subset) ? $subset->name : '',
  );

  $form['wrapper']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler: Save subset.
 */
function field_manager_subset_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['subset'])) {
    $values['id'] = $values['subset']->id;
    $values['updated'] = time();
    $primary_keys = array('id');
  }
  else {
    $values['created'] = time();
    $primary_keys = array();
  }

  drupal_write_record('field_manager_subsets', $values, $primary_keys);
  drupal_set_message(t('The subset has been saved.', array(), array('context' => 'custom')));

  drupal_goto('admin/structure/field-manager');
}

/**
 * Form callback: Delete subset.
 */
function field_manager_subset_delete_form($form, $form_state, $subset) {
  $form = array();
  $form['#subset'] = $subset;

  return confirm_form($form, t('Are you sure you want to delete this subset'), 'admin/structure/field-manager', t('This action can not be undone.'), t('Delete'), t('Cancel'), 'field_manager_subset_delete_form_submit');
}

/**
 * Form submission handler: Delete the actual subset.
 */
function field_manager_subset_delete_form_submit($form, &$form_state) {
  db_delete('field_manager_subsets')
    ->condition('id', $form['#subset']->id)
    ->execute();

  drupal_set_message(t('The subset has been removed.', array(), array('context' => 'custom')));
  drupal_goto('admin/structure/field-manager');
}

/**
 * Form callback: Subset field form.
 */
function field_manager_subset_field_form($form, $form_state, $subset) {
  $form = array();

  $form['subset'] = array(
    '#type' => 'value',
    '#value' => $subset,
  );

  $fields = field_manager_fields($subset->content_type, 'node', 'form');
  $default_values = field_manager_subset_fields($subset->id);

  $defaults = array();
  foreach ($default_values as $key => $default_value) {
    if ($default_value) {
      $defaults[$key] = $key;
    }
  }

  $options = array();
  foreach ($fields as $key => $field) {
    if (isset($field['label'])) {
      $options[$key] = $field['label'];
    }
  }

  $form['fields'] = array(
    '#title' => t('Fields'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $defaults,
  );

  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 * Form submission callback: Save field configuration.
 */
function field_manager_subset_field_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];

  // Remove previous entries.
  db_delete('field_manager_subset_fields')->condition('subset_id', $values['subset']->id)->execute();

  foreach ($values['fields'] as $field_name => $status) {
    $value = array(
      'subset_id' => $values['subset']->id,
      'field_name' => $field_name,
      'status' => ($status) ? 1: 0,
    );

    drupal_write_record('field_manager_subset_fields', $value);
  }

  drupal_set_message(t('The subset fields has been saved.', array(), array('context' => 'custom')));
  drupal_goto('admin/structure/field-manager');
}

/**
 * Helper function: Field manager subsets.
 */
function field_manager_subsets() {
  $query = db_select('field_manager_subsets', 's')
            ->fields ('s')
            ->orderBy('content_type')
            ->orderBy('name')
            ->execute();
  $subsets = $query->fetchAll();

  return $subsets;
}

/**
 * Helper function: Retrieve all fields and groups of a specific content type.
 */
function field_manager_fields($bundle, $entity_type = 'node', $view_mode = 'form') {
  $instances = field_manager_entity_fields($bundle, $entity_type);

  $fields = array();
  foreach ($instances as $key => $field) {
    $instance = field_info_instance('node', $field->field_name, $bundle);

    if (isset($instance['label']) && !empty($instance['label'])) {
      $fields[$field->field_name] = $instance;
    }

    // If field is a field_collection fetch them fields also.
    if ($instance['widget']['module'] == 'field_collection') {
      $collection_bundle = $instance['field_name'];
      $collection_instances = field_manager_entity_fields($collection_bundle, $instance['widget']['module']);

      foreach ($collection_instances as $collection_instance) {
        $collection_instance = field_info_instance('field_collection_item', $collection_instance->field_name, $collection_bundle);

        // Transform label to add field collection name.
        $collection_instance['label'] = $collection_instance['label'] . ' ' . t('(Part of @name)', array('@name' => $instance['label']));

        $fields[$collection_instance['field_name']] = $collection_instance;
      }
    }
  }

  return $fields;
}

/**
 * Helper function: Retreive all field instances of a bundle.
 */
function field_manager_entity_fields($bundle, $entity_type = 'node') {
  $options = array('target' => 'slave');

  $sql = "SELECT field_name
          FROM field_config_instance ci
          WHERE ci.bundle = :content_type";

  $result = db_query($sql, array(':content_type' => $bundle), $options);
  return $result->fetchAll();
}

/**
 * Form callback: Role fields form.
 */
function field_manager_role_fields_form($form, $form_state, $role = NULL, $type) {
  $form = array();

  $types = field_manager_visibility_contexts();
  $fields = field_manager_fields($type->type, 'node', 'form');

  $form['#content_type'] = $type->type;
  $form['#types'] = $types;
  $form['#role'] = $role;

  $form['matrix'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  foreach ($fields as $field_name => $field) {
    $form['matrix'][$field_name] = array(
      '#type' => 'container',
      '#tree' => TRUE,
    );

    foreach ($types as $key => $val) {
      $form['matrix'][$field_name][$key] = array(
        '#type' => 'checkbox',
        '#default_value' => field_manager_access($key, $field_name, $type->type, $role->rid),
      );
    }
  }

  $form['#theme'] = 'field_manager_field_matrix_form';

  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 * Form submission callback: Role fields form.
 */
function field_manager_role_fields_form_submit(&$form, &$form_state) {
  $values = $form_state['values']['matrix'];

  // Delete previous entries.
  db_delete('field_manager_role_fields')
    ->condition('rid', $form['#role']->rid)
    ->condition('entity_type', $form['#content_type'])
    ->execute();

  foreach ((array) $values as $field_name => $types) {
    foreach ($types as $type => $status) {
      $value = array(
        'field_name' => $field_name,
        'rid' => $form['#role']->rid,
        'entity_type' => $form['#content_type'],
        'type' => $type,
        'status' => $status,
      );

      drupal_write_record('field_manager_role_fields', $value);
    }
  }

  drupal_set_message(t('The fields per role configuration has been saved.', array(), array('context' => 'custom')));
  drupal_goto('admin/structure/field-manager');
}

/**
 * Theme callback: Matrix form.
 */
function theme_field_manager_field_matrix_form($form) {
  $types = $form['form']['#types'];
  $fields = field_manager_fields($form['form']['#content_type'], 'node', 'form');

  $header = array(
    array('data' => t('Field')),
  );

  foreach ($types as $key => $type) {
    $header[] = array('data' => $type);
  }

  $form = $form['form'];
  $rows = array();
  foreach ($fields as $field_name => $field) {
    $row = array();
    $row[] = array('data' => check_plain($field['label']));

    foreach ($types as $key => $type) {
      $row[] = array('data' => drupal_render($form['matrix'][$field_name][$key]));
    }

    $rows[] = $row;
  }

  $themed = theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));

  return $themed . drupal_render_children($form);
}
